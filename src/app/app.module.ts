import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PassStrengthBarModule } from './passApp/pass-strength-bar.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    PassStrengthBarModule
  ],
  providers: [ ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
