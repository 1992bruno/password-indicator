import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http';
import { HttpRequest } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpResponse } from '@angular/common/http';
import 'rxjs/add/observable/of';

@Injectable()
export class FakeBackendService implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return  request.url.endsWith('/pass-strength') ?
      Observable.of(new HttpResponse({ status: 200, body: measureStrength(request.body) })) : next.handle(request);
  }
}

function measureStrength(p) {
  var strength = 0;
  if (!p) {
    return strength;
  } else {
    var letters = new Object();
    for (let i = 0; i < p.length; i++) {
      letters[p[i]] = (letters[p[i]] || 0) + 1;
      strength += 7.0 / letters[p[i]];
    }

    var regex = /[$-/:-?{-~!"^_`\[\]]/g;
    var lowerLetters = /[a-z]+/.test(p);
    var upperLetters = /[A-Z]+/.test(p);
    var numbers = /[0-9]+/.test(p);
    var symbols = regex.test(p);

    var flags = [lowerLetters, upperLetters, numbers, symbols];

    var passedMatches = 0;
    for (const check in flags) {
      if (flags.hasOwnProperty(check)) {
        passedMatches += (flags[check] === true) ? 1 : 0;
      }
    }
    strength += (passedMatches - 1) * 5;
    if (passedMatches > 3 && p.length > 10) {
      strength *= 1.5;
    }
    return Math.round(strength / 10);
  }
}

export var fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendService,
  multi: true
};
