import { Component, DoCheck } from '@angular/core';
import { PassStrengthBarService } from './pass-strength-bar.service';


@Component({
  selector: 'app-pass-strength-bar',
  templateUrl: './pass-strength-bar.component.html',
  styleUrls: ['./pass-strength-bar.component.scss']
})
export class PassStrengthBarComponent implements DoCheck {

  public bars = [];
  public strength: number;
  public password = "";



  constructor(private pass: PassStrengthBarService) {
    for (var i = 0; i < 12; i++) {
      this.bars.push(i);
    }
  }
/* onPaste(e: any) {
    var content = confirm("Paste password?")
      if (!content) {
         e.preventDefault();
      }
    } */
  ngDoCheck() {
    return this.pass.getStrength(this.password).subscribe(
      data => { this.strength = Number(data); },
      err => { console.log(err); }
    );
  }


  private getColor(barId) {
    var unitColor = [].fill('#DDD', 0, 11);
    for (var i = 0; i < this.strength; i++) {
      if (barId > 7) {
        unitColor[i] = '#0F0';
      }
      else
        if (barId > 3) {
        unitColor[i] = '#F90';
        }
        else {
          unitColor[i] = '#F00';
        }
    }
    return unitColor[barId];
  }

}
