import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { fakeBackendProvider } from '../backend/backend.service';
import { PassStrengthBarService } from './pass-strength-bar.service';
import { PassStrengthBarComponent } from './pass-strength-bar.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [ PassStrengthBarComponent ],
  providers: [ fakeBackendProvider, PassStrengthBarService ],
  exports: [ PassStrengthBarComponent ]
})
export class PassStrengthBarModule { }
