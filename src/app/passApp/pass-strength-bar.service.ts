import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PassStrengthBarService {
  constructor(private http: HttpClient) {}

  getStrength(password: string) {
    return this.http.post('/pass-strength', password, httpOptions);
  }
}
